const { Router } = require('express');
const axios = require('axios');
const HttpStatus = require('http-status-codes');
const appConfig = require('../app-config.json');
const { isUserLogged } = require('../helpers/user');
const ResponseHelper = require('../helpers/response');

const router = Router();
for (const service of Object.values(appConfig.services)) {
  router.get(`/${service.slug}`, (req, res, next) => {
    if (!isUserLogged(req, res)) {
      return;
    }

    axios.get(`${service.url}/backup`, {
      params: {
        token: appConfig.token,
      },
    })
      .then(() => {
        req.session.successMessage = `Zadanie tworzenia kopii ${service.name} zostało rozpoczęte`;
        res.redirect('/panel');
        res.end();
      })
      .catch((error) => {
        next(ResponseHelper.createError(HttpStatus.BAD_REQUEST, `Error creating backup for ${service.name}. Reason: ${error.message}`));
      });
  });
}

module.exports = router;
