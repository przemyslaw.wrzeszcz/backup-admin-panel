const { Router } = require('express');
const appConfig = require('../app-config.json');
const { isUserLogged } = require('../helpers/user');
const { sessionMessages, fetchServicesStatus } = require('../helpers/utils');

const router = Router();

/* GET index page. */
router.get('/', (req, res) => {
  if (isUserLogged(req, res, false)) {
    res.redirect('/panel');
    res.end();

    return;
  }

  res.render('index', sessionMessages(req.session));
});

router.post('/auth', (req, res) => {
  const { username, password } = req.body;

  if (!(username && password)) {
    req.session.errorMessage = 'Please enter Username and Password!';
    res.redirect('/');
    res.end();
    return;
  }

  if (username === appConfig.username && password === appConfig.password) {
    req.session.loggedin = true;
    res.redirect('/panel');
    res.end();
    return;
  }

  req.session.errorMessage = 'Incorrect Username and/or Password!';
  res.redirect('/');
  res.end();
});

router.get('/logout', (req, res) => {
  if (!isUserLogged(req, res)) {
    return;
  }

  req.session.loggedin = false;
  res.redirect('/');
  res.end();
});

router.get('/panel', (req, res) => {
  if (!isUserLogged(req, res)) {
    return;
  }

  // TODO: Handle messages in template
  // TODO: Fetch and show backups statuses
  // TODO: disable backup button if status for service exists

  fetchServicesStatus().then((statuses) => {
    res.render('panel', {
      services: Object.values(appConfig.services),
      servicesStatus: statuses,
      servicesStatusKeys: Object.keys(statuses),
      ...sessionMessages(req.session),
    });
  });
});

module.exports = router;
