module.exports = {
  apps: [
    {
      name: 'tewu-admin-panel',
      script: './bin/start',
      watch: false,
      env: {
        NODE_ENV: 'production',
      },
    },
  ],
};
