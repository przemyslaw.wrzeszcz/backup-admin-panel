const axios = require('axios');
const moment = require('moment');
const isEmpty = require('locutus/php/var/empty');
const appConfig = require('../app-config.json');

module.exports = {
  sessionMessages: (session) => {
    const msgTypes = ['error', 'info', 'success'];
    const prepared = {};

    for (const type of msgTypes) {
      const key = `${type}Message`;
      prepared[key] = (session[key]) ? session[key] : '';

      if (session[key]) {
        // eslint-disable-next-line no-param-reassign
        session[key] = null;
      }
    }

    return prepared;
  },
  fetchServicesStatus: () => {
    const { services } = appConfig;
    const requestsTable = [];

    for (const service of Object.values(services)) {
      requestsTable.push(
        axios.get(`${service.url}/status`, {
          params: {
            token: appConfig.token,
          },
        }),
      );
    }

    return axios.all(requestsTable)
      .then((responseArr) => {
        const result = {};

        for (const response of responseArr) {
          const { slug, birthtime } = response.data;

          if (isEmpty(services[slug])) {
            throw new Error(`Otrzymano status z nieznanej usługi "${slug}"`);
          }

          if (birthtime === null) {
            continue;
          }

          const date = moment(birthtime);
          result[slug] = `${services[slug].name}: Kopia w toku. Tworzenie kopii bezpieczeństwa rozpoczęte ${date.format('HH:mm:SS DD-MM-YYYY')}r.`;
        }

        return result;
      }).catch((err) => ({
        error: `Błąd pobierania statusów usług. Komunikat błędu: ${err.message}`,
      }));
  },
};
