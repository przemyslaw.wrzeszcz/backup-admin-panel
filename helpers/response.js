const HttpStatus = require('http-status-codes');

class ResponseHelper {
  static createError(code, message = null) {
    const finalMessage = message === null ? HttpStatus.getStatusText(code) : message;
    const customError = new Error(finalMessage);
    customError.code = code;

    return customError;
  }

  /**
   * Return Error
   *
   * @param {ServerResponse} response
   * @param {object} data object with data
   */
  static errorResponse(response, data) {
    const errorData = {
      status: data.code,
      detail: data.message,
    };

    return ResponseHelper.modelResponse(response, errorData, data.code);
  }

  /**
   * Return response
   *
   * @param {ServerResponse} response
   * @param {object} data object with data
   * @param {integer} code response code
   */
  static modelResponse(response, data, code = HttpStatus.OK) {
    response.writeHead(code, {
      'Content-Type': 'application/json',
    });

    response.end(JSON.stringify(data));
  }
}

module.exports = ResponseHelper;
