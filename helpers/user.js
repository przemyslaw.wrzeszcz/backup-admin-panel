module.exports = {
  isUserLogged: (req, res, redirect = true) => {
    if (!req.session.loggedin) {
      if (redirect) {
        res.redirect('/');
        res.end();
      }

      return false;
    }

    return true;
  },
};
