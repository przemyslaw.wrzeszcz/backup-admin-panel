const cookieParser = require('cookie-parser');
const express = require('express');
const session = require('express-session');
const hbs = require('hbs');
const FileStore = require('session-file-store')(session);
const httpErrors = require('http-errors');
const logger = require('morgan');
const path = require('path');
const handlebarsHelpers = require('./helpers/handlebars');

const indexRouter = require('./routes/index');
const backupsRouter = require('./routes/backups');

for (const helperName of Object.keys(handlebarsHelpers)) {
  hbs.registerHelper(helperName, handlebarsHelpers[helperName]);
}

const app = express();

app.use(session({
  store: new FileStore({}),
  secret: 'TeWu secret cat',
  resave: true,
  saveUninitialized: true,
}));

app.set('views', path.join(__dirname, 'views'));
// view engine setup
app.set('view engine', 'hbs');
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/backups', backupsRouter);

// catch 404 and forward to error handler
app.use((req, res, next) => {
  next(httpErrors(404));
});

// error handler
app.use((err, req, res, next) => {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || err.code || 500);
  res.render('error', {
    title: 'Express',
  });
});

module.exports = app;
